import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': 'Bearer BQCAB1rJ71OYcROsFn1fUVJnIPm_SnTvrzc8kH4P-7y1QcGFvJjhzr5Uslrqy-tRDBj06x75E8FJQ4jSDSM'
    });

    return this.http.get(url, { headers });
  }

  getNewReleases() {

    return this.getQuery('browse/new-releases?limit=20')
      .pipe( map( data => {
        return data['albums'].items;
      }));

  }

  getArtistas(termino: string) {

    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
      .pipe( map( data => data['artists'].items ));
  }

  getArtista(id: string) {
    return this.getQuery( `artists/${ id }` );
  }

  getTopTracks(id: string) {
    return this.getQuery( `artists/${ id }/top-tracks?country=ES` ).pipe(
      map( data => data['tracks'] ));
  }

}
